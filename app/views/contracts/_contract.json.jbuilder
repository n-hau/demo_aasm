json.extract! contract, :id, :name, :state, :created_at, :updated_at
json.url contract_url(contract, format: :json)
