class Contract < ApplicationRecord
  include AASM

  aasm :column => 'state' do
    state :new, initial: true
    state :dealt
    state :paid
    state :ended

    after_all_transitions :log_status_change
    event :deal do
      transitions from: :new, to: :dealt
    end

    event :undeal do
      transitions from: :dealt, to: :new
    end

    event :pay do
      transitions from: :dealt, to: :paid
    end

    event :return_money do
      transitions from: :paid, to: :dealt
    end
    
    event :end do
      transitions from: :paid, to: :ended
    end

    event :renew do
      transitions from: :ended, to: :new
    end
  end

  def log_status_change
    puts "============================SAU KHI TẤT CẢ CÁC TRANSTION ĐƯỢC THỰC HIỆN==============================="
    puts "Thay đổi từ --> #{aasm.from_state} <-- tới --> #{aasm.to_state} <-- (state: --> #{aasm.current_event} <--)"
    puts "======================================================================================================"
  end
end
