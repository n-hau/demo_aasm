class ContractsController < ApplicationController
  before_action :set_contract, only: [:show, :edit, :update, :destroy, :thoa_thuan, :huy_keo, :tra_tien, :ket_thuc, :tra_lai_tien, :tao_moi]

  # GET /contracts
  # GET /contracts.json
  def index
    @contracts = Contract.all
  end

  # GET /contracts/1
  # GET /contracts/1.json
  def show
  end

  # GET /contracts/new
  def new
    @contract = Contract.new
  end

  # GET /contracts/1/edit
  def edit
  end

  # POST /contracts
  # POST /contracts.json
  def create
    @contract = Contract.new(contract_params)

    respond_to do |format|
      if @contract.save
        format.html { redirect_to @contract, notice: 'Contract was successfully created.' }
        format.json { render :show, status: :created, location: @contract }
      else
        format.html { render :new }
        format.json { render json: @contract.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contracts/1
  # PATCH/PUT /contracts/1.json
  def update
    respond_to do |format|
      if @contract.update(contract_params)
       
        format.html { redirect_to @contract, notice: 'Contract was successfully updated.' }
        format.json { render :show, status: :ok, location: @contract }
      else
        format.html { render :edit }
        format.json { render json: @contract.errors, status: :unprocessable_entity }
      end
    end
  end

  def thoa_thuan
    respond_to do |format|
      if @contract.deal!
        format.html { redirect_to contracts_path, notice: "Hợp đồng số ##{@contract.id} đã thỏa thuận" }
      end
    end
  end

  def huy_keo
    respond_to do |format|
      if @contract.undeal!
        format.html { redirect_to contracts_path, notice: "Hợp đồng số ##{@contract.id} đã bị hủy kèo" }
      end
    end
  end

  def tra_tien
    respond_to do |format|
      if @contract.pay!
        format.html { redirect_to contracts_path, notice: "Hợp đồng số ##{@contract.id} đã thanh toán" }
      end
    end
  end

  def tra_lai_tien
    respond_to do |format|
      if @contract.return_money!
        format.html { redirect_to contracts_path, notice: "Hợp đồng số ##{@contract.id} đã bị đòi lại tiền" }
      end
    end
  end

  def ket_thuc
    respond_to do |format|
      if @contract.end!
       format.html { redirect_to contracts_path, notice: "Hợp đồng số ##{@contract.id} đã kết thúc" }
      end
    end
  end

  def tao_moi
    respond_to do |format|
      if @contract.renew!
        format.html { redirect_to contracts_path, notice: "Hợp đồng số ##{@contract.id} đã được tạo mới" }
      end
    end
  end

  def destroy
    @contract.destroy
    respond_to do |format|
      format.html { redirect_to contracts_url, notice: 'Contract was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_contract
      @contract = Contract.find(params[:id])
    end

    def contract_params
      params.require(:contract).permit(:name)
    end
end
