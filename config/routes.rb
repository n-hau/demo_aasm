Rails.application.routes.draw do
  resources :contracts do
    member do
      get 'thoa_thuan'
      get 'tra_tien'
      get 'ket_thuc'
      get 'huy_keo'
      get 'tra_lai_tien'
      get 'tao_moi'
      patch 'thoa_thuan', to: 'contracts#thoa_thuan'
      patch 'huy_keo', to: 'contracts#huy_keo'
      patch 'tra_tien', to: 'contracts#tra_tien'
      patch 'tra_lai_tien', to: 'contracts#tra_lai_tien'
      patch 'ket_thuc', to: 'contracts#ket_thuc'
      patch 'tao_moi', to: 'contracts#tao_moi'
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
